<?php
/* Plugin Name: WP Reactor
 * Plugin URI: logicsbuffer.com
 * Description: Place this Shortcode in header.php "[show_design_tool]" to make it work.
 * Author: WpRight and Shery Faseh
 * Author URI: https://www.fiverr.com/wpright
 * Version: 1.0
 */
add_action( 'init', 'design_tool' );

function design_tool() {
	add_shortcode( 'show_design_tool', 'design_tool_form' );
	add_shortcode( 'show_cropper_tool', 'design_cropper_tool' );
	add_action( 'wp_enqueue_scripts', 'fabric_scripts' );
	//include 'wp-facebook-login/facebook-login.php';
	//include 'slider/slider.php';
}
function fabric_scripts() {
	wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );
global $wp_query;
	wp_localize_script( 'love', 'postlove', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'postID' => $wp_query->post->ID,
		));
		wp_enqueue_script( 'fabric_js', plugins_url() . '/reactor/js/fabric.js' );
		//wp_enqueue_script( 'design_tool_js', plugins_url() . '/reactor/js/design_tool.js' );
		wp_enqueue_script( 'html2canvas_js', plugins_url() . '/reactor/js/html2canvas.js' );
		wp_enqueue_script( 'slider_js', plugins_url() . '/reactor/js/slider.js' );

		wp_enqueue_style( 'design_tool_css', plugins_url() . '/reactor/css/design_tool.css',array(),time() );
}
add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
function post_love_add_love() {
	global $current_user;
	get_currentuserinfo();
	$upload_dir = wp_upload_dir();
	$user_dirname = $upload_dir['basedir'].'/'.$current_user->user_login;
	if ( ! file_exists( $user_dirname ) ) {
		wp_mkdir_p( $user_dirname );
	}
	$img_64 = $_REQUEST['canvas_img'];
	$emoji_reaction = $_REQUEST['emoji_reaction'];
	$design_name = $_REQUEST['design_name'];
	$file_type='.jpeg';
	
	$path = $user_dirname.'/'.$design_name.$file_type;
	print_r($path);
	print_r($design_name);
	$ifp = fopen($path, "wb"); 
    $data = explode(',', $img_64);
    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp);	
	
	
	
	//Insert Post 
	global $post;
	
	if($path){
		$user_name = $current_user->user_login;
		$uniq_id = uniqid();

		// Create post object				
			$my_post = array(
			  'post_title'    => "Tile $uniq_id",
			  'post_type'  => "product",
			  'post_content'  => "test",
			  'post_status'   => 'publish'
			);
		
		// Insert the post into the database
		$parent_post_id = wp_insert_post( $my_post );
	}
	$user_id = get_post_field( 'post_author', $parent_post_id );
	//$fb_url = get_avatar_url( $user_id, 72 );
	$fb_img = get_usermeta($user_id, 'facebook_avatar_thumb');
	$size = 72;
	$fb_id = get_user_meta( $user_id, '_fb_user_id', true );
	if ( $fb_id = get_user_meta( $user_id, '_fb_user_id', true ) ) {
		$fb_url = 'https://graph.facebook.com/' . $fb_id . '/picture?width=' . $size . '&height=' . $size;
	}
	$reaction_id = "reaction_11";
	$category_terms = array( $reaction_id );
	wp_set_object_terms( $parent_post_id, $category_terms , 'product_cat' );
	//wp_set_post_terms( $parent_post_id, array('metais'), $category_texonomy);
	 //update_post_meta( $parent_post_id, 'reaction', $emoji_reaction );
	 //update_post_meta( $parent_post_id, 'fb_url', $fb_url );

	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $path ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $path ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $path ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $path, $parent_post_id );
	update_field('custom_tiles_attach', $attach_id, $parent_post_id);
	$attach_url = wp_get_attachment_image( $attach_id,'full');
	update_field('custom_tiles', $attach_url, $parent_post_id);

	//set_post_thumbnail( $parent_post_id, $attach_id );	
	$uniq_id = uniqid();
	$mmr_price = 20;
	$skuu = 'tile'.$uniq_id;
	//$parent_post_id = get_the_ID();
	update_post_meta($parent_post_id, '_sku', $skuu );
	update_post_meta( $parent_post_id, '_price', $mmr_price );
	update_post_meta( $parent_post_id,'_regular_price', $mmr_price );
	global $woocommerce;
	$woocommerce->cart->add_to_cart($parent_post_id);
	$cart_url = site_url().'/cart/';
	wp_redirect( $cart_url );
	exit;
}

function login_redirect( $redirect_to, $request, $user ){
    return home_url('my-account/');
}
add_filter( 'login_redirect', 'login_redirect', 10, 3 );


function design_cropper_tool() {
					
	?>
  <script src="https://foliotek.github.io/Croppie/croppie.js"></script>

		<div id="adjust_image_div" style="display: none;">
			<div class="adjust_content">
				<div class=" text-center">
					<div class="top-bar">
						<div class="button-cross">X</div>
						<div class="heading-adjust">ADJUST</div>
						<div class="btn btn-success upload-result">Done</div>
					</div>
					<div id="upload-demo" style="width:350px"></div>
				</div>
				<div class="" style="padding-top:30px;">					
					<input type="text" id="frame_current_index" value="" style="display:none;">
					<input type="text" id="img_data" value="" style="display:none;" name="feature_img_data">
				</div>
				<!--
				<div class="col-md-4" style="">
					<div id="upload-demo-i" style="display:none;background:#e1e1e1;width:300px;padding:30px;height:300px;margin-top:30px"></div>
				</div> -->
	<!--<img id="featured_image" src="https://tiles.logicsbuffer.com/wp-content/uploads/2020/03/tech-43.jpg" />-->

			</div>
		</div>


<!-- or even simpler -->
<script type="text/javascript">

jQuery('.upload-result').on('click', function (ev) {
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: {
		width: 400
	},
	}).then(function (resp) {
		
var frame_index = jQuery("#frame_current_index").val();
	jQuery('#preview1img-multi-6-0-'+frame_index).attr('src',resp);
	jQuery("#img_data").val(resp);

	});
});


</script>
<?php
}
function design_tool_form() {
ob_start();
/* $plugin_dir = plugin_dir_path( __FILE__ );
$dir = $plugin_dir.'images';	
$emoji = Get_ImagesToFolder($dir);
natsort($emoji); */
//$emoji = array_slice($emoji, 0, 10);
echo show_slider();
?>
<!--<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>-->
		<a href="#reaction_window_parent" id=""/><div id="rt_react"><button id="click_react" class="button">Checkout</button></div></a>
	<!-- Trigger/Open The Modal -->

	<div id="previewImage" style="display:none;">
	</div>
	<div id="preloader" style="display:none;">
	<img scr="http://drcolorchipsa.co.za/wp-content/uploads/2017/12/ajax_loader_gray_512.gif">
	</div>


<!-- The Modal -->

	
	<!-- CSS Model -->
	<!-- <a href="#reaction_window_parent">Open Modal</a> -->
	<div style="display: none;"><canvas style="display:none;" id="canvas_tiles"></canvas></div>
	

<?php 
design_cropper_tool();
$checkout_url = site_url()."/checkout"; ?>
<script>

jQuery(document).ready(function(){	  
var uniqid = '<?php echo uniqid(); ?>';
var checkout_url = '<?php echo $checkout_url; ?>';
console.log(checkout_url);
//jQuery("#jssor_1").hide();
	jQuery("#rt_react").on('click', function () {
		var canvas = document.getElementById('canvas_tiles');
		//if (jQuery('body').hasClass('logged-in')) {
		    //execute your jquery code.
		    //console.log("Loggedin");	
			
			html2canvas(jQuery('#frames_output'), {
				onrendered: function(canvas) {
					jQuery('#canvas_tiles').replaceWith(canvas);
					var canvasData = canvas.toDataURL("image/png");
					//var canvasData = localStorage.getItem("canvas_image");
					var design_name = 'design'+uniqid;
					console.log(canvasData);
					//var postData = "canvasData="+canvasData;
					//var debugConsole= document.getElementById("debugConsole");
					localStorage.setItem("react_check",1);
					jQuery.ajax({
						url : postlove.ajax_url,
						type : 'post',
						data : {
							action : 'post_love_add_love',
							canvas_img : canvasData,
							design_name : design_name
						},
						success : function( response ) {
							window.open(checkout_url,'_self');
						}
					});	
					return false;
				},
				//width: 200,
				//height: 200

			});
			 
			

		//}else{
			//console.log("Not Loggedin");
			//jQuery("#reaction_window_parent").show();
			//jQuery("#reaction_window").hide();
			//alert("Please Login First to React.");
	//	}
		
		
		//jQuery("#reaction_window_parent").show();
		//jQuery("#previewImage").html(canvas1);
	});
jQuery('.insert-emo').click(function() {
	

	
});

});
</script>
<?php
$result = ob_get_clean();
return $result;
}

function show_slider() {
?>
	<!-- custom 3d Slider.
    <script src="http://reactor.logicsbuffer.com/wp-content/plugins/reactor/3d-slider/js/slider.js"></script> -->
   

<?php
}

//Get Emojis form folder code

$plugin_dir = plugin_dir_path( __FILE__ );
$dir = $plugin_dir.'images';
//$ImagesA = Get_ImagesToFolder($dir);
//print_r($ImagesA);
//echo $dir;

function Get_ImagesToFolder($dir){

if (is_dir($dir)){
  if ($dh = opendir($dir)){
    while (($file = readdir($dh)) !== false){
      $images[] = $file;
    }
    closedir($dh);
  }
return $images;
}

}

add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
function order_meta_handler($item_id, $values, $cart_item_key) {
	$cart_session = WC()->session->get('cart');
	if($cart_session[$cart_item_key]['custom_tiles'][0]){
	wc_add_order_item_meta($item_id, "Custom tiles", $cart_session[$cart_item_key]['custom_tiles']);
	}
	if($cart_session[$cart_item_key]['estimated_days'][0]){
	wc_add_order_item_meta($item_id, "Estimated days", $cart_session[$cart_item_key]['estimated_days'][0]);
	}
	
}
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);

function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	global $woocommerce;
	//$custom_tiles = get_feild('custom_tiles', $new_post_id);
	$custom_tiles = get_post_meta($new_post_id, 'custom_tiles',true);
	$cart_item_meta['custom_tiles'] = $custom_tiles;
	$cart_item_meta['estimated_days'] = $estimated_days;
	return $cart_item_meta;
}

