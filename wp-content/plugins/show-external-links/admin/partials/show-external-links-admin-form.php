<?php

/**
 * Used to mark up the form in the admin panel of the plugin
 *
 * @since      1.0.0
 * @package    Show_External_Links
 * @subpackage Show_External_Links/admin/partials
 */


echo '
<form action="" method="post">
    <h2>' . __( 'Show External Links Settings', 'show-external-links' ) . '</h2>
        <h4>' . __( 'Skip sites', 'show-external-links' ) . '</h4>
            <p>
                <textarea name="sel_skip_sites" rows="15" cols="100" placeholder="' . __( 'Domains or links, all from a new line.', 'show-external-links' ) . PHP_EOL . PHP_EOL . __( 'For example:', 'show-external-links' ) . PHP_EOL . 'example.com' . PHP_EOL . 'example.org' . PHP_EOL . PHP_EOL . __( 'or with parameters:', 'show-external-links' ) . PHP_EOL . 'https://example.com/page/' . PHP_EOL . 'https://example.org/?param=page' . PHP_EOL . PHP_EOL . __( 'URLs and parameters are truncated, only the domain remains.', 'show-external-links' ) . '
                ">' . $option_value . '</textarea>
            </p>
            <p>
                <input type="submit" value="' . __( 'Submit', 'show-external-links' ) . '">
            </p>
</form>';

 	//echo "sdfsdfsdf";
	//$test12 = get_option( 'final_result' );
	//echo $test12;
	
	//Custom
	$args = array(
	  'numberposts' => 100
	);

	$latest_posts = get_posts( $args );
	$post_content = '';
	foreach($latest_posts as $post){
		$post_content .=$post->post_content;
	}
	//print_r($post_content);

	$html = '<p>'.$post_content.'</p>';
	//$html = '<p>Welcome to WordPress. This <a href="https://gvalighting.com/">sdfsd</a>is your first post. Edit or delete it, then start writing!</p>';
	$sel_skip_sites = ( is_multisite() ) ? get_site_option( 'sel_skip_sites' ) : get_option( 'sel_skip_sites' );
$links_pattern = '/(<a(?![^>]+class=\"ab-item\")[^>]+((https?:\/\/|www)(?!(' . addslashes( $sel_skip_sites) . '))[\w\.\/\-=?#]+)[^>]+>)(.*?)<\/a>/ui';
	$html = str_replace( [ "\t", "\n", "\r" ], '', $html );
	preg_match_all( $links_pattern, $html, $out );

	$list_of_links   = array_map( function( $link ) {
		return wp_parse_url( $link, PHP_URL_HOST );
	}, $out[2] );
	$repetition_rate = array_count_values( $list_of_links );
	$str_links       = '';

	foreach ( $repetition_rate as $key => $value ) {
		$str_links .= $key . ' (' . $value . ')<br />';
	}
	/* 
?><pre><?php print_r($str_links); ?></pre><?php

	$html_tse = '<p>Welcome to WordPress. This <a href="https://gvalighting.com/">sdfsd</a>is your first post. Edit or delete it, then start writing!</p>';
	$html = preg_replace( $links_pattern, '\1<span class="external-links" data-tooltip="\2">\5</span></a>', $html, -1, $count );
	$sel_skip_sites = is_multisite() ? get_site_option( 'sel_skip_sites' ) : get_option( 'sel_skip_sites' );
$note           = ( false === $sel_skip_sites || empty( $sel_skip_sites ) ) ? '' : '<br /><span class="sel-note">(' . __( 'except for excluded sites', 'show-external-links' ) . ')</span>';

$popup = '           
<input class="open-selinks" id="top-box-selinks" type="checkbox" hidden>
<label class="btn-selinks" for="top-box-selinks"></label>
<div class="top-panel-selinks"> 
   <div class="message-selinks">
       <h2>' . __( 'External links found!', 'show-external-links' ) . '</h2> 
       <div class="Table">
           <div class="row-collection">
               <div class="Table-row">
                   <div class="Table-row-item">
                       <p>
                       ' . sprintf( _n( 'On this page %s link', 'There are %s links on this page',
            $number_of_links, 'show-external-links' ), $number_of_links ) . ':' . $note . '
                       </p>
                   </div>
                   <div class="Table-row-item">
                       <p class="list-of-links">' . $list_of_links . '</p>
                   </div>
               </div>
          </div>
      </div>
      <p class="check-source">
      ' . __( 'If you do not see all the links on the page, check the source code.', 'show-external-links' ) . '
      </p>
  </div>
</div>';
	
	$html = preg_replace( '/<p([^>]*?)>/ui', '<p\1>' .$popup, $html ); */

   
	//update_option( 'final_result', $html );
	echo $str_links;